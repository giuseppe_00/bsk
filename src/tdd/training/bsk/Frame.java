package tdd.training.bsk;

public class Frame {

	private int FirstThrow;
	private int SecondThrow;
	private int frameBonus=0;
	
	
	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		FirstThrow=firstThrow;
		SecondThrow=secondThrow;
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		
		return FirstThrow;
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		
		return SecondThrow;
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
		if(isSpare() || isStrike())
		{
			frameBonus=bonus;
		}
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		// To be implemented
		return frameBonus;
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {
		if(isSpare() || isStrike())
		{
			return FirstThrow + SecondThrow + getBonus();
		}
		else
		{
			return FirstThrow + SecondThrow;
		}
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
		// To be implemented
		if(FirstThrow==10 && SecondThrow==0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		// To be implemented
		if((FirstThrow + SecondThrow)==10 && SecondThrow!=0)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}

}
