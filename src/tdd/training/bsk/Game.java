package tdd.training.bsk;

import java.util.ArrayList;

public class Game {

	private int indexThrow=0;
	private ArrayList<Frame> g;
	private int firstBonus=0;
	private int secondBonus=0;
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		g=new ArrayList<>();
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		
			g.add(indexThrow, frame);
			indexThrow++;
		
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) {
		// To be implemented
		return g.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		// To be implemented
	    firstBonus=firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		// To be implemented
		secondBonus=secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		// To be implemented
		return firstBonus;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		// To be implemented
		return secondBonus;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException{
		int score=0;
		for(int i = 0; i < g.size(); i++) {
			 checkStrike(i);   
			 checkSpare(i);
			 score += getFrameAt(i).getScore();  
	       }
		return score;	
	}
	
	public void checkSpare(int i){
	
		int bonus=0;
		if(getFrameAt(i).isSpare())
		{
			if(i==g.size()-1)
			{
				bonus= getFirstBonusThrow();
			    getFrameAt(i).setBonus(bonus);
			}
			else
			{
				bonus= getFrameAt(i+1).getFirstThrow();
			    getFrameAt(i).setBonus(bonus);
			}
			
		}
	}
	public void checkStrike(int i) {
		
		int bonus=0;
		if(getFrameAt(i).isStrike())
		{
			
			if(i==g.size()-1)
			{
				
				bonus= getFirstBonusThrow() + getSecondBonusThrow();
			    getFrameAt(i).setBonus(bonus);
			}
			else
			{
				if(getFrameAt(i+1).isStrike())
				{
					if(i==8) {
						bonus= getFrameAt(i+1).getFirstThrow() + getFirstBonusThrow();
						getFrameAt(i).setBonus(bonus);
					}
					else
					{
						bonus= getFrameAt(i+1).getFirstThrow() + getFrameAt(i+1).getSecondThrow()+getFrameAt(i+2).getFirstThrow();
					    getFrameAt(i).setBonus(bonus);
					}
				}
				else
				{
					bonus= getFrameAt(i+1).getFirstThrow() + getFrameAt(i+1).getSecondThrow();
					getFrameAt(i).setBonus(bonus);
				}
			}
		}
	}

}
