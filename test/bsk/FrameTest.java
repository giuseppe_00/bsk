package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.Frame;


public class FrameTest {

	@Test
	public void testInput() throws Exception{
		int num1=2, num2=4;
		Frame frame=new Frame(num1,num2);
		assertEquals(2,frame.getFirstThrow());
		assertEquals(4,frame.getSecondThrow());
	}
	
	@Test
	public void testScore() throws Exception{
		int num1=2, num2=6;
		Frame frame=new Frame(num1,num2);
		assertEquals(8,frame.getScore());
	}
	
	@Test
	public void testIsSpare() throws Exception{
		int num1=1, num2=9;
		Frame frame=new Frame(num1,num2);
		assertTrue(frame.isSpare());
	}
	
	@Test
	public void testGetBonus() throws Exception{
		int num1=1, num2=9,bonus=3;
		Frame frame=new Frame(num1,num2);
		frame.setBonus(bonus);
		assertEquals(bonus,frame.getBonus());
	}
	
	@Test
	public void testScoreBonus() throws Exception{
		int num1=1, num2=9,bonus=3;
		Frame frame=new Frame(num1,num2);
		frame.setBonus(bonus);
		assertEquals(13,frame.getScore());
	}
	
	@Test
	public void testIsStrike() throws Exception{
		int num1=10, num2=0;
		Frame frame=new Frame(num1,num2);
		assertTrue(frame.isStrike());
	}
	@Test
	public void testScoreStrike() throws Exception{
		int num1=10, num2=0,bonus=9;
		Frame frame=new Frame(num1,num2);
		frame.setBonus(bonus);
		assertEquals(19,frame.getScore());
	}

}
