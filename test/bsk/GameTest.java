package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {

	
	@Test
	public void testGetFrameAt()  throws Exception{
		int first, second;
		Frame fra=new Frame(2,4);
		Game game=new Game();
		game.addFrame(new Frame(1,4));
	    game.addFrame(new Frame(3,4));
	    game.addFrame(new Frame(0,8));
	    game.addFrame(new Frame(4,7));
	    game.addFrame(new Frame(2,3));
	    game.addFrame(new Frame(9,1));
	    game.addFrame(new Frame(2,4));
	    game.addFrame(new Frame(2,4));
	    game.addFrame(new Frame(3,8));
	    game.addFrame(new Frame(2,4));
	    Frame fr=game.getFrameAt(9);
	    first=fra.getFirstThrow();
	    second=fra.getSecondThrow();
	    assertEquals(first,fr.getFirstThrow());
	    assertEquals(second,fr.getSecondThrow());
	}
	
	@Test
	public void testScoreSpare()  throws Exception{
		Game game=new Game();
		game.addFrame(new Frame(1,9));
	    game.addFrame(new Frame(3,6));
	    game.addFrame(new Frame(7,2));
	    game.addFrame(new Frame(3,6));
	    game.addFrame(new Frame(4,4));
	    game.addFrame(new Frame(5,3));
	    game.addFrame(new Frame(3,3));
	    game.addFrame(new Frame(4,5));
	    game.addFrame(new Frame(8,1));
	    game.addFrame(new Frame(2,6));
	    
	    assertEquals(88,game.calculateScore());
	}
	
	@Test
	public void testScoreStike()  throws Exception{
		Game game=new Game();
		game.addFrame(new Frame(10,0));
	    game.addFrame(new Frame(3,6));
	    game.addFrame(new Frame(7,2));
	    game.addFrame(new Frame(3,6));
	    game.addFrame(new Frame(4,4));
	    game.addFrame(new Frame(5,3));
	    game.addFrame(new Frame(3,3));
	    game.addFrame(new Frame(4,5));
	    game.addFrame(new Frame(8,1));
	    game.addFrame(new Frame(2,6));
	    
	    assertEquals(94,game.calculateScore());
	}
	
	@Test
	public void testScoreStikeSpare()  throws Exception{
		Game game=new Game();
		game.addFrame(new Frame(10,0));
	    game.addFrame(new Frame(4,6));
	    game.addFrame(new Frame(7,2));
	    game.addFrame(new Frame(3,6));
	    game.addFrame(new Frame(4,4));
	    game.addFrame(new Frame(5,3));
	    game.addFrame(new Frame(3,3));
	    game.addFrame(new Frame(4,5));
	    game.addFrame(new Frame(8,1));
	    game.addFrame(new Frame(2,6));
	    
	    assertEquals(103,game.calculateScore());
	}
	
	@Test
	public void testScoreMultipleStike()  throws Exception{
		Game game=new Game();
		game.addFrame(new Frame(10,0));
	    game.addFrame(new Frame(10,0));
	    game.addFrame(new Frame(7,2));
	    game.addFrame(new Frame(3,6));
	    game.addFrame(new Frame(4,4));
	    game.addFrame(new Frame(5,3));
	    game.addFrame(new Frame(3,3));
	    game.addFrame(new Frame(4,5));
	    game.addFrame(new Frame(8,1));
	    game.addFrame(new Frame(2,6));
	    
	    assertEquals(112,game.calculateScore());
	}
	
	@Test
	public void testScoreMultipleSpare()  throws Exception{
		Game game=new Game();
		game.addFrame(new Frame(8,2));
	    game.addFrame(new Frame(5,5));
	    game.addFrame(new Frame(7,2));
	    game.addFrame(new Frame(3,6));
	    game.addFrame(new Frame(4,4));
	    game.addFrame(new Frame(5,3));
	    game.addFrame(new Frame(3,3));
	    game.addFrame(new Frame(4,5));
	    game.addFrame(new Frame(8,1));
	    game.addFrame(new Frame(2,6));
	    
	    assertEquals(98,game.calculateScore());
	}
	
	@Test
	public void testScoreLastSpare()  throws Exception{
		Game game=new Game();
		int bonus=7;
		game.addFrame(new Frame(1,5));
	    game.addFrame(new Frame(3,6));
	    game.addFrame(new Frame(7,2));
	    game.addFrame(new Frame(3,6));
	    game.addFrame(new Frame(4,4));
	    game.addFrame(new Frame(5,3));
	    game.addFrame(new Frame(3,3));
	    game.addFrame(new Frame(4,5));
	    game.addFrame(new Frame(8,1));
	    game.addFrame(new Frame(2,8));
	    
	    game.setFirstBonusThrow(bonus);
	    assertEquals(90,game.calculateScore());
	}
	
	@Test
	public void testScoreLastStrike()  throws Exception{
		Game game=new Game();
		int bonus=7, bonus2=2;
		game.addFrame(new Frame(1,5));
	    game.addFrame(new Frame(3,6));
	    game.addFrame(new Frame(7,2));
	    game.addFrame(new Frame(3,6));
	    game.addFrame(new Frame(4,4));
	    game.addFrame(new Frame(5,3));
	    game.addFrame(new Frame(3,3));
	    game.addFrame(new Frame(4,5));
	    game.addFrame(new Frame(8,1));
	    game.addFrame(new Frame(10,0));
	    
	    game.setFirstBonusThrow(bonus);
	    game.setSecondBonusThrow(bonus2);
	    assertEquals(92,game.calculateScore());
	}
	
	@Test
	public void testBestScore()  throws Exception{
		Game game=new Game();
		int bonus=10, bonus2=10;
		game.addFrame(new Frame(10,0));
	    game.addFrame(new Frame(10,0));
	    game.addFrame(new Frame(10,0));
	    game.addFrame(new Frame(10,0));
	    game.addFrame(new Frame(10,0));
	    game.addFrame(new Frame(10,0));
	    game.addFrame(new Frame(10,0));
	    game.addFrame(new Frame(10,0));
	    game.addFrame(new Frame(10,0));
	    game.addFrame(new Frame(10,0));
	    
	    game.setFirstBonusThrow(bonus);
	    game.setSecondBonusThrow(bonus2);
	    assertEquals(300,game.calculateScore());
	    
	}
}
